package il.co.hyperactive.gpsstatus;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;


public class GPSStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle("GPS Status update")
                .setContentText("GPS Status changed")
                .setSmallIcon(android.R.drawable.ic_delete)
                .setColor(Color.BLUE)
                .build();
        nm.notify(0,notification);
    }
}
